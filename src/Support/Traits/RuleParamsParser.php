<?php

namespace Itwmw\Validate\Support\Traits;

use Itwmw\Validate\Support\Common;

/**
 * 规则参数解析器
 */
trait RuleParamsParser
{
    /**
     * 解析參數中的方法，定义格式：{#function}
     *
     * @see https://v.itwmw.com/6/RuleParamsParser.html#%E6%96%B9%E6%B3%95%E8%B0%83%E7%94%A8 文档
     *
     * @param array $params
     *
     * @return array
     */
    public function parseFunction(array $params): array
    {
        foreach ($params as &$param) {
            if (!is_string($param)) {
                continue;
            }
            if (str_starts_with($param, '{#') && str_ends_with($param, '}')) {
                $function = substr($param, 2, -1);
                $_params  = [];
                if (str_contains($function, '(') && str_ends_with($function, ')')) {
                    $functionInfo = explode('(', substr($function, 0, -1), 2);
                    $function     = $functionInfo[0];
                    $_params      = explode(',', $functionInfo[1]);
                }
                if (str_contains($function, '::')) {
                    $function = explode('::', $function);
                    if (is_callable([$function[0], $function[1]])) {
                        $param = call_user_func_array([$function[0], $function[1]], $_params);
                        continue;
                    }
                }
                if (is_callable([$this, $function])) {
                    $param = call_user_func_array([$this, $function], $_params);
                    continue;
                }

                if (is_callable($function)) {
                    $param = call_user_func_array($function, $_params);
                }
            }
        }

        return $params;
    }

    /**
     * 解析参数中的对象属性，定义格式: {->property}
     *
     * 多级类引用使用“->”分隔，如：{->user->name}
     * 数组引用，使用“.”分割，如：{->user.info->nickname}
     *
     * @see https://v.itwmw.com/6/RuleParamsParser.html#%E5%B1%9E%E6%80%A7%E8%8E%B7%E5%8F%96 文档
     *
     * @param array $params 待解析的参数数组，其中可能包含特定格式的对象属性引用
     *
     * @return array 解析后的参数数组，其中的属性引用已被替换为实际属性值
     */
    public function parseProperty(array $params): array
    {
        foreach ($params as &$param) {
            if (!is_string($param)) {
                continue;
            }
            if (preg_match_all('/\{->([^\}]+)\}/', $param, $matches)) {
                foreach ($matches[1] as $index => $field) {
                    $param = str_replace($matches[0][$index], $this->getDataByPath($this, '->' . $field), $param);
                }
            }
        }

        return $params;
    }

    /**
     * 根据给定的路径获取对象或数组中的数据。
     *
     * @param object $class 起始搜索的对象
     * @param string $path  数据的路径，使用'->'访问对象属性，使用'.'访问数组元素。
     *
     * @return mixed 返回找到的数据，如果路径不存在则返回null
     */
    protected function getDataByPath(object $class, string $path): mixed
    {
        $parts = preg_split('/(\->|\.)/', $path, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        if (1 === count($parts) % 2) {
            return $path;
        }
        $data = $class;
        for ($i = 0; $i < count($parts); $i += 2) {
            $key   = $parts[$i];
            $value = $parts[$i + 1];

            if ('->' === $key) {
                if (!is_object($data) || !property_exists($data, $value)) {
                    return null;
                }
                $data = $data->$value;
            } elseif ('.' === $key) {
                if (!is_array($data) || !array_key_exists($value, $data)) {
                    return null;
                }
                $data = $data[$value];
            } else {
                return null;
            }
        }

        return $data;
    }

    /**
     * 解析參數中的环境变量，定义格式: {@env}
     *
     * @see https://v.itwmw.com/6/RuleParamsParser.html#%E7%8E%AF%E5%A2%83%E5%8F%98%E9%87%8F 文档
     *
     * @param array $params
     *
     * @return array
     */
    public function parseEnvironment(array $params): array
    {
        foreach ($params as &$param) {
            if (!is_string($param)) {
                continue;
            }
            if (preg_match_all('/\{@([^\}]+)\}/', $param, $matches)) {
                foreach ($matches[1] as $index => $field) {
                    $param = str_replace($matches[0][$index], getenv($field), $param);
                }
            }
        }
        return $params;
    }

    /**
     * 解析參數中的字段引用，定义格式: {:field}
     *
     * @see https://v.itwmw.com/6/RuleParamsParser.html#%E5%AD%97%E6%AE%B5%E5%BC%95%E7%94%A8 文档
     *
     * @param array $params
     *
     * @return array
     */
    public function parseFieldData(array $params): array
    {
        foreach ($params as &$param) {
            if (!is_string($param)) {
                continue;
            }
            if (preg_match_all('/\{\:([^\}]+)\}/', $param, $matches)) {
                foreach ($matches[1] as $index => $field) {
                    $param = str_replace($matches[0][$index], data_get($this->withCheckData, $field), $param);
                }
            }
        }
        return $params;
    }

    /**
     * 解析参数
     *
     * @see https://v.itwmw.com/6/RuleParamsParser.html 文档
     *
     * @param array|null $params
     *
     * @return array|null
     */
    protected function parseParams(?array $params): ?array
    {
        $params = parent::parseParams($params);
        if (empty($params)) {
            return $params;
        }
        $params = $this->parseProperty($params);
        $params = $this->parseFieldData($params);
        $params = $this->parseEnvironment($params);
        return $this->parseFunction($params);
    }

    /**
     * 解析规则 - 处理规则中的参数
     *
     * @param $rule
     * @param string $field
     * @return array|mixed|string
     */
    protected function parseRule($rule, string $field): mixed
    {
        $rule = parent::parseRule($rule, $field);

        if (is_string($rule)) {
            [$ruleName, $ruleParams] = $this->getKeyAndParam($rule, true);
        } elseif (is_array($rule)) {
            $ruleName = $rule[0];
            if (!is_string($ruleName)) {
                return $rule;
            }
            $ruleParams = array_slice($rule, 1);
        } else {
            return $rule;
        }

        $ruleParams = $this->parseParams($ruleParams);
        if (is_array($ruleParams)) {
            return [$ruleName, ...$ruleParams];
        }
        return $ruleName;
    }

    protected function getKeyAndParam(string $value, bool $parsing = false): array
    {
        [$key, $param] = parent::getKeyAndParam($value);

        if ($parsing && !is_null($param)) {
            $param = preg_split('/,(?![^()]*\))/', $param);
        }
        return [$key, $param];
    }
}
