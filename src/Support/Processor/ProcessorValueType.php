<?php

namespace Itwmw\Validate\Support\Processor;

enum ProcessorValueType implements ProcessorSupport
{
    /**
     * 表明这是一个函数，闭包
     */
    case FUNCTION;

    /**
     * 表明这是一个类方法
     */
    case METHOD;

    /**
     * 表明这是一个值，不需要解析为函数等
     */
    case VALUE;

    /**
     * 表明需要自定推导
     */
    case AUTO;
}
