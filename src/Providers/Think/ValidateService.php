<?php

namespace Itwmw\Validate\Providers\Think;

use Itwmw\Validate\Support\Storage\ValidateConfig;
use Itwmw\Validation\Factory;
use think\facade\Config;
use think\Service;

class ValidateService extends Service
{
    public function register()
    {
        $factory = new Factory(null, str_replace('-', '_', Config::get('lang.default_lang')));
        $factory->setPresenceVerifier(new PresenceVerifier());
        ValidateConfig::instance()->setFactory($factory);
    }
}
