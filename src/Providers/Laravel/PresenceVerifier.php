<?php


/**
 * @noinspection PhpUndefinedClassInspection
 * @noinspection PhpUndefinedNamespaceInspection
 */

namespace Itwmw\Validate\Providers\Laravel;

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Query\Builder;
use Itwmw\Validation\Support\Interfaces\PresenceVerifierInterface;

class PresenceVerifier implements PresenceVerifierInterface
{
    protected Builder $db;

    protected DatabaseManager $connectionResolver;

    public function __construct(DatabaseManager $db)
    {
        $this->connectionResolver = $db;
    }

    public function table(string $table): PresenceVerifierInterface
    {
        $this->db = $this->connectionResolver->table($table)->useWritePdo();
        return $this;
    }

    public function setConnection($connection)
    {
        $this->connectionResolver->connection($connection);
        return $this;
    }

    public function where(string $column, $operator = null, $value = null, string $boolean = 'and'): PresenceVerifierInterface
    {
        $this->db->where($column, $operator, $value, $boolean);
        return $this;
    }

    public function whereIn(string $column, $values, string $boolean = 'and'): PresenceVerifierInterface
    {
        $this->db->whereIn($column, $values, $boolean);
        return $this;
    }

    public function count(string $columns = '*'): int
    {
        return $this->db->count($columns);
    }

    public function newQuery(): void
    {
        if (isset($this->db)) {
            $this->db = $this->db->newQuery();
        }

    }
}
