<?php

namespace Itwmw\Validate\Tests\Material;

use Itwmw\Validate\Support\Storage\ValidateConfig;
use Itwmw\Validation\Factory;
use PHPUnit\Framework\TestCase;

class BaseTestValidate extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        $factory = new Factory();
        ValidateConfig::instance()->setFactory($factory)
            ->setRulesPath('Itwmw\\Validate\\Tests\\Material\\Rules\\');

        parent::__construct($name, $data, $dataName);
    }
}
