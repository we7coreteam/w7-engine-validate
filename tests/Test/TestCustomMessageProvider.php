<?php

namespace Itwmw\Validate\Tests\Test;

use Itwmw\Validate\Exception\ValidateRuntimeException;
use Itwmw\Validate\Support\Concerns\MessageProviderInterface;
use Itwmw\Validate\Support\MessageProvider;
use Itwmw\Validate\Tests\Material\BaseTestValidate;
use Itwmw\Validate\Validate;

class TestMessageProvider extends MessageProvider implements MessageProviderInterface
{
}

class TestCustomMessageProvider extends BaseTestValidate
{
    /**
     *  测试多种方式设置消息处理器
     *
     * @throws \Itwmw\Validate\Exception\ValidateException
     */
    public function testSetMessageProvider()
    {
        Validate::make()->setMessageProvider(TestMessageProvider::class);
        Validate::make()->setMessageProvider(new TestMessageProvider());
        Validate::make()->setMessageProvider(function () {
            return new TestMessageProvider();
        });

        $this->expectException(ValidateRuntimeException::class);
        Validate::make()->setMessageProvider('Test');
    }

    public function testGetMessage()
    {
        $messageProvider = new TestMessageProvider();
        $messageProvider->setData([
            'user' => 'admin',
            'pass' => '123456'
        ]);

        $messageProvider->setCustomAttributes([
            'user' => '账号',
            'pass' => '密码'
        ]);

        $message = $messageProvider->handleMessage('{@user}:{:user},{@pass}:{:pass}');
        $this->assertEquals('账号:admin,密码:123456', $message);
    }

    /**
     *  测试错误消息中对数组的支持
     *
     * @return void
     */
    public function testMessageArrayIndex()
    {
        $messageProvider = new TestMessageProvider();
        $messageProvider->setData([
            'data' => [
                [], [
                    'b' => [
                        [
                            'c' => '123'
                        ]
                    ]
                ]
            ]
        ]);

        $messageProvider->setAttribute('data.1.b.0.c');
        $message = $messageProvider->handleMessage('第{&&0}个数组,索引为{&0}出错了：{:data.*.b.*.c}');
        $this->assertEquals('第2个数组,索引为1出错了：123', $message);
    }

    /**
     *  测试用户为错误消息添加了关键词处理
     *
     * @return void
     */
    public function testMessageRender()
    {
        $messageProvider = new TestMessageProvider();
        $messageProvider->render(':name', function () {
            return '虞灪';
        });

        $message = $messageProvider->handleMessage('当前名称不可与预设名称一致，触发预设名称: :name');
        $this->assertEquals('当前名称不可与预设名称一致，触发预设名称: 虞灪', $message);
    }
}
