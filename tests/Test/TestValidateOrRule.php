<?php

namespace Itwmw\Validate\Tests\Test;

use Itwmw\Validate\Exception\ValidateException;
use Itwmw\Validate\Tests\Material\BaseTestValidate;
use Itwmw\Validate\Tests\Material\Rules\IsAdminRule;
use Itwmw\Validate\Validate;
use Itwmw\Validation\Support\Rule\AndRule;

class TestValidateOrRule extends BaseTestValidate
{
    protected function checkHandler(Validate $v)
    {
        $data = $v->check([
            'username' => '995645888@qq.com'
        ]);
        $this->assertSame('995645888@qq.com', $data['username']);

        $data = $v->check([
            'username' => '18888889999'
        ]);
        $this->assertSame('18888889999', $data['username']);

        $this->expectException(ValidateException::class);
        $this->expectExceptionMessage('Username不符合要求。');
        $v->check([
            'username' => '123456'
        ]);
    }

    /**
     *  测试在rule中直接定义or规则
     */
    public function testOrRule()
    {
        $v = new class extends Validate {
            protected $rule = [
                'username' => 'required|or:email,mobile'
            ];
        };

        $this->checkHandler($v);
    }

    /**
     *  测试or规则使用规则组
     */
    public function testOrRuleUseRuleGroup()
    {
        $v = new class extends Validate {
            protected $group = [
                'username' => 'email|mobile'
            ];

            protected $rule = [
                'username' => 'required|or:username'
            ];
        };

        $this->checkHandler($v);
    }

    /**
     *  测试or规则使用规则组和规则结合
     */
    public function testOrRuleUseRuleAndRuleGroup()
    {
        $v = new class extends Validate {
            protected $group = [
                'username' => 'email|mobile'
            ];

            protected $rule = [
                'username' => 'required|or:username,chs'
            ];
        };

        $data = $v->check([
            'username' => '虞灪'
        ]);
        $this->assertSame('虞灪', $data['username']);
        $this->checkHandler($v);
    }

    /**
     *  测试or规则使用多个规则组
     */
    public function testOrRuleUseMultipleRule()
    {
        $v = new class extends Validate {
            protected $group = [
                'username'      => 'email|mobile',
                'usernameOther' => [
                    'chs', 'alpha'
                ]
            ];

            protected $rule = [
                'username' => 'required|or:username,usernameOther'
            ];
        };

        $data = $v->check([
            'username' => '虞灪'
        ]);
        $this->assertSame('虞灪', $data['username']);

        $data = $v->check([
            'username' => 'Yepyuyu'
        ]);
        $this->assertSame('Yepyuyu', $data['username']);
        $this->checkHandler($v);
    }

    /**
     * 测试Or规则和规则组以及正则表达式规则的组合
     */
    public function testOrRuleAndRegexRule()
    {
        $v = new class extends Validate {
            protected $regex = [
                'mobile' => '/^1[3-9]\d{9}$/',
                'chs'    => '/^[\x{4e00}-\x{9fa5}]+$/u'
            ];

            protected $group = [
                'username_format' => 'email|regex:mobile'
            ];

            protected $rule = [
                'username' => [
                    'required', 'or:username_format,regex:chs'
                ]
            ];
        };

        $data = $v->check([
            'username' => '虞灪'
        ]);
        $this->assertSame('虞灪', $data['username']);

        $this->checkHandler($v);
    }

    /**
     * 测试Or规则中使用AndRule规则
     */
    public function testOrRuleAndAndRule()
    {
        $v = new class extends Validate {
            public function __construct()
            {
                $this->group = [
                    'usernameEmail' => new AndRule([
                        'email', 'max:20'
                    ])
                ];
            }

            protected $rule = [
                'username' => 'required|or:usernameEmail,chs'
            ];
        };

        $data = $v->check([
            'username' => '虞灪'
        ]);
        $this->assertSame('虞灪', $data['username']);

        $data = $v->check([
            'username' => '995645888@qq.com'
        ]);
        $this->assertSame('995645888@qq.com', $data['username']);

        $this->expectException(ValidateException::class);
        $this->expectExceptionMessage('Username不符合要求。');
        $v->check([
            'username' => '995645888888888888@qq.com'
        ]);
    }

    /**
     * 测试在Or规则中使用自定义规则
     */
    public function testOrRuleWithCustomRule()
    {
        $v = new class extends Validate {
            protected $rule = [
                'username' => 'required|or:email,mobile,isAdmin'
            ];

            public function ruleIsAdmin($attribute, $value): bool
            {
                return 'admin' === $value;
            }
        };

        $data = $v->check([
            'username' => 'admin'
        ]);
        $this->assertSame('admin', $data['username']);

        $this->checkHandler($v);
    }

    /**
     * 测试在Or规则中使用自定义规则类
     */
    public function testOrRuleWithCustomRuleClass()
    {

        $v = new class extends Validate {
            protected $rule = [
                'username' => 'required|or:email,mobile,' . IsAdminRule::class
            ];
        };

        $data = $v->check([
            'username' => 'admin'
        ]);
        $this->assertSame('admin', $data['username']);

        $this->checkHandler($v);
    }
}
