<?php

namespace Itwmw\Validate\Tests\Test;

use Itwmw\Validate\Exception\ValidateException;
use Itwmw\Validate\Support\Concerns\ProcessorInterface;
use Itwmw\Validate\Support\DataAttribute;
use Itwmw\Validate\Support\Processor\ProcessorExecCond;
use Itwmw\Validate\Support\Processor\ProcessorOptions;
use Itwmw\Validate\Support\Processor\ProcessorParams;
use Itwmw\Validate\Support\ValidateScene;
use Itwmw\Validate\Tests\Material\BaseTestValidate;
use Itwmw\Validate\Validate;

class SetPreprocessorIsHello implements ProcessorInterface
{
    public function handle($value, string $attribute, array $originalData)
    {
        return 'Hello';
    }
}

class TestDataPreprocessor extends BaseTestValidate
{
    public function testPreprocessorIsScalar()
    {
        $v = new class extends Validate {
            protected $rule = [
                'name' => 'required'
            ];

            protected $preprocessor = [
                'name' => ['123', ProcessorExecCond::WHEN_EMPTY]
            ];
        };

        $data = $v->check([]);

        $this->assertSame('123', $data['name']);
    }

    public function testPreprocessorIsArray()
    {
        $v = new class extends Validate {
            protected $rule = [
                'name' => 'required'
            ];

            protected $preprocessor = [
                'name' => [['a', 'b', 'any' => 123]]
            ];
        };

        $data = $v->check([]);

        $this->assertEquals(['a', 'b', 'any' => 123], $data['name']);

        $v = new class extends Validate {
            protected $rule = [
                'name' => 'required'
            ];

            protected $preprocessor = [
                'name' => [['a', 'b']]
            ];
        };

        $data = $v->check([]);

        $this->assertEquals(['a', 'b'], $data['name']);
    }

    public function testPreprocessorIsCallback()
    {
        $v = new class extends Validate {
            protected $rule = [
                'name' => 'required',
                'age'  => 'required|numeric',
                'sex'  => 'required'
            ];

            public function __construct()
            {
                $this->preprocessor = [
                    'name' => function ($value) {
                        return '小张';
                    },
                    'age'  => [[$this, 'setAge'], ProcessorParams::Value],
                    'sex'  => 'setSex'
                ];
            }

            public function setAge($value)
            {
                return 100;
            }

            public function setSexProcessor($value)
            {
                return '男';
            }
        };

        $data = $v->check([]);

        $this->assertEquals(['name' => '小张', 'age' => 100, 'sex' => '男'], $data);
    }

    public function testHandlerData()
    {
        $v = new class extends Validate {
            protected $rule = [
                'id' => 'required'
            ];

            public function __construct()
            {
                $this->preprocessor = [
                    'id' => [function ($value) {
                        if (is_string($value)) {
                            return explode(',', $value);
                        }
                        return $value;
                    }, ProcessorParams::Value]
                ];
            }
        };

        $data = $v->check([
            'id' => '1,2,3,4'
        ]);

        $this->assertEquals([1, 2, 3, 4], $data['id']);
    }

    public function testPreprocessorForScene()
    {
        $v = new class extends Validate {
            protected $rule = [
                'name' => 'required'
            ];

            protected function sceneTest(ValidateScene $scene)
            {
                $scene->only(['name'])
                    ->preprocessor('name', '小张');
            }
        };

        $data = $v->scene('test')->check([]);
        $this->assertSame('小张', $data['name']);

        $this->expectException(ValidateException::class);
        $v->check([]);
    }

    public function testCancelPreprocessorValue()
    {
        $v = new class extends Validate {
            protected $rule = [
                'name' => ''
            ];

            protected $preprocessor = [
                'name' => [1, ProcessorExecCond::WHEN_EMPTY]
            ];

            protected function sceneTest(ValidateScene $scene)
            {
                $scene->only(['name'])
                    ->preprocessor('name', null);
            }
        };

        $data = $v->check([]);
        $this->assertEquals(1, $data['name']);

        $data = $v->scene('test')->check([]);
        $this->assertArrayNotHasKey('name', $data);
    }

    public function testPreprocessorUsePreprocessorClass()
    {
        $v = new class extends Validate {
            protected $rule = [
                'name' => ''
            ];

            protected $preprocessor = [
                'name' => SetPreprocessorIsHello::class
            ];
        };

        $data = $v->check([]);
        $this->assertSame('Hello', $data['name']);
    }

    public function testPreprocessorDeleteField()
    {
        $v = new class extends Validate {
            protected $rule = [
                'a' => 'numeric'
            ];

            protected $preprocessor = [
                'a' => 'deleteField'
            ];

            public function deleteFieldProcessor($value, $field, $data, DataAttribute $dataAttribute)
            {
                $dataAttribute->deleteField = true;
                return '';
            }
        };

        $data = $v->check([
            'a' => 123
        ]);

        $this->assertTrue(empty($data));
    }

    public function testPreprocessorBuiltInFunction()
    {
        $v = new class extends Validate {
            protected $rule = [
                'a' => 'required'
            ];

            protected $preprocessor = [
                'a' => ['trim', ProcessorParams::Value]
            ];
        };

        $data = $v->check([
            'a' => '     12312  asd asd      '
        ]);

        $this->assertSame('12312  asd asd', $data['a']);
    }

    public function testPreprocessorCustomFunction()
    {
        $v = new class extends Validate {
            protected $rule = [
                'a' => 'required|string'
            ];

            protected $preprocessor = [
                'a' => ['test', ProcessorParams::Attribute, ProcessorParams::Value, ProcessorParams::Attribute]
            ];

            public function testProcessor(string $att, string $value, string $attribute)
            {
                return $att . '-' . $value . '-' . $attribute;
            }
        };

        $data = $v->check([
            'a' => 'test'
        ]);

        $this->assertSame('a-test-a', $data['a']);
    }

    public function testPreprocessorWhenValueIsEmpty()
    {
        $v = new class extends Validate {
            protected $rule = [
                'a' => 'nullable|string'
            ];

            protected $preprocessor = [
                'a' => ['trim', ProcessorParams::Value, ProcessorExecCond::WHEN_NOT_EMPTY]
            ];
        };

        $data = $v->check([]);

        $this->assertNull($data['a'] ?? null);

        $data = $v->check([
            'a' => '  ates '
        ]);

        $this->assertSame('ates', $data['a']);
    }

    public function testPreprocessorInCustomScene()
    {
        $v = new class extends Validate {
            protected $rule = [
                'test' => 'nullable|string'
            ];

            protected function sceneTest(ValidateScene $scene)
            {
                $scene->only(['test'])
                    ->postprocessor('test', 'intval', ProcessorParams::Value, ProcessorExecCond::WHEN_NOT_EMPTY);
            }
        };

        $data = $v->scene('test')->check([
            'test' => '123'
        ]);

        $this->assertSame(123, $data['test']);

        $data = $v->scene('test')->check([
            'test' => ''
        ]);

        $this->assertSame('', $data['test']);
    }

    public function testMultiplePreprocessor()
    {
        $v = new class extends Validate {
            protected $rule = [
                'test' => 'nullable|string'
            ];

            protected $preprocessor = [
                'test' => [
                    ['trim', ProcessorParams::Value],
                    ['base64_encode', ProcessorParams::Value],
                    ProcessorOptions::MULTIPLE
                ],
            ];
        };

        $data = $v->check([
            'test' => '  test  '
        ]);

        $this->assertSame('dGVzdA==', $data['test']);
    }

    public function testMultiplePreprocessorInScene()
    {
        $v = new class extends Validate {
            protected $rule = [
                'test' => 'nullable|string'
            ];

            protected function sceneMultiple(ValidateScene $scene)
            {
                $scene->only(['test'])
                    ->preprocessor('test', 'trim', ProcessorParams::Value)
                    ->preprocessor('test', 'base64_encode', ProcessorParams::Value);
            }
        };

        $data = $v->check([
            'test' => '  test  '
        ]);

        $this->assertSame('  test  ', $data['test']);

        $data = $v->scene('multiple')->check([
            'test' => '  test  '
        ]);

        $this->assertSame('dGVzdA==', $data['test']);
    }

    public function testSetFilterForArrayField()
    {
        $v = new class extends Validate {
            protected $rule = [
                'id'   => 'required|array',
                'id.*' => 'numeric'
            ];

            protected $preprocessor = [
                'id.*' => ['intval', ProcessorParams::Value]
            ];
        };

        $data = $v->check(['id' => ['1', '2', 3, '4']]);

        foreach ($data['id'] as $id) {
            $this->assertSame('integer', gettype($id));
        }
    }

    public function testSetDefaultForDataExistAndEmpty()
    {
        $v = new class extends Validate {
            protected $rule = [
                'sort' => ''
            ];

            public function sceneEmpty(ValidateScene $scene): void
            {
                $scene->only(true)
                    ->preprocessor('sort', 1, ProcessorExecCond::WHEN_EMPTY);
            }

            public function sceneExistAndEmpty(ValidateScene $scene): void
            {
                $scene->only(true)
                    ->preprocessor('sort', 1, ProcessorExecCond::WHEN_EXIST, ProcessorExecCond::WHEN_EMPTY);
            }

            public function sceneNotExist(ValidateScene $scene): void
            {
                $scene->only(true)
                    ->preprocessor('sort', 1, ProcessorExecCond::WHEN_NOT_EXIST);
            }
        };

        $data = $v->check([]);
        $this->assertEmpty($data);

        $data = $v->scene('empty')->check([]);
        $this->assertEquals(1, $data['sort']);

        $data = $v->scene('existAndEmpty')->check([]);
        $this->assertEmpty($data);

        $data = $v->scene('existAndEmpty')->check(['sort' => '']);
        $this->assertEquals(1, $data['sort']);

        $data = $v->scene('existAndEmpty')->check(['sort' => 100]);
        $this->assertEquals(100, $data['sort']);

        $data = $v->scene('notExist')->check(['sort' => 100]);
        $this->assertEquals(100, $data['sort']);

        $data = $v->scene('notExist')->check(['sort' => '']);
        $this->assertEquals('', $data['sort']);

        $data = $v->scene('notExist')->check([]);
        $this->assertEquals(1, $data['sort']);
    }
}
